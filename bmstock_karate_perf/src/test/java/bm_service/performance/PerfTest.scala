package performance

import com.intuit.karate.gatling.PreDef._
import io.gatling.core.Predef._
import scala.concurrent.duration._

class PerfTest extends Simulation {

  val protocol = karateProtocol(
    "/api/stocks/{stockId}" -> Nil
  )

  protocol.nameResolver = (req, ctx) => req.getHeader("karate-name")

  val login = scenario("login").exec(karateFeature("classpath:bm_service/feature/user_login.feature"))
  val stock = scenario("stock").exec(karateFeature("classpath:bm_service/feature/stock.feature"))

  setUp(
    login.inject(rampUsers(10) during (60 seconds)).protocols(protocol),
    stock.inject(rampUsers(5) during (60 seconds)).protocols(protocol)
  )

}
