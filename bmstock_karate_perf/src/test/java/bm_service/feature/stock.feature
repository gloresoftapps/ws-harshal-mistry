Feature: Tests for Stock

    Background: Define URL
        Given url stocksUrl
    
    # Stock search - start
    Scenario: Search stock success
        Given path 'SBICARD.NS'
        When method Get
        Then status 200
        And match response == {"yearLow": '#number',"previousClose":'#number',"stockName":"SBICARD.NS","lastAccessTime":"#notnull","price":'#number',"id":0,"userId":0,"yearHigh":'#number',"open":'#number'}
    
    Scenario: Search empty stock name
        Given path ''
        When method Get
        Then status 400

    Scenario: Search invalid stock name
        Given path 'ADSFAFFAFAS'
        When method Get
        Then status 404
        And match response.errors[0] == 'No stock found for - ADSFAFFAFAS'
    # Stock search - end

    # Stock add - start
    Scenario: Add stock to watchlist success
        And request { "userId": 99999, "stockName": "IDEA.NS" }
        When method Post
        Then status 201
    
    Scenario: Add stock to watchlist invalid userId
        And request { "userId": -1, "stockName": "IDEA.NS" }
        When method Post
        Then status 400
        And match response.errors[0] == 'Please provide valid userId'

    Scenario: Add stock to watchlist empty userId
        And request { "userId": "", "stockName": "IDEA.NS" }
        When method Post
        Then status 400
        And match response.errors[0] == 'Please provide valid userId'

    Scenario: Add stock to watchlist missing userId
        And request { "stockName": "IDEA.NS" }
        When method Post
        Then status 400
        And match response.errors[0] == 'Please provide valid userId'

    Scenario: Add stock to watchlist empty stockName
        And request { "userId": 99999, "stockName": "" }
        When method Post
        Then status 400
        And match response.errors[0] == 'Please provide stockName'

    Scenario: Add stock to watchlist missing stockName
        And request { "userId": 99999 }
        When method Post
        Then status 400
        And match response.errors[0] == 'Please provide stockName'

    Scenario: Add stock to watchlist empty body values
        And request { "userId": "", "stockName": "" }
        When method Post
        Then status 400
        And match response.errors == '#[2]'

    Scenario: Add stock to watchlist empty body 
        And request { }
        When method Post
        Then status 400
        And match response.errors == '#[2]'
    # Stock add - end

    # Stock get watchlist - start
    Scenario: Get watchlist stock
        Given param userId = 99999
        When method Get
        And status 200
        And match response == '#[1]'
        And match response[0].current == '#number'
        And match response[0].twoWeeksGain == '#number'
        And match response[0].stockName == 'IDEA.NS'
        And match response[0].todayGain == '#number'
        And match response[0].threeWeeksGain == '#number'
        And match response[0].id == '#number'
        And match response[0].lastWeekGain == '#number'
        And match response[0].monthGain == '#number'

    Scenario: Get watchlist stock invalid userId
        Given param userId = -1
        When method Get
        And status 400
        And match response.errors[0] contains 'Please provide valid userId'
    # Stock get watchlist - end

    # Stock update watchlist - start
    Scenario: Update stock note success
        Given param userId = 99999
        When method Get
        And status 200
        * print response
        * def stockId = response[0].id;
        
        Given path '' + stockId
        And request { "userId": "99999", "stockName": "IDEA.NS", "userNote": "buy 10 stocks after week"}
        Given method Put
        And status 200

        Given param userId = 99999
        When method Get
        And status 200
        * print response
        And match response[0].userNote == 'buy 10 stocks after week'

        Given path '' + stockId
        Given method Delete
        And status 200

        Given param userId = 99999
        When method Get
        And status 200
        And match response == '#[0]'
    # Stock update watchlist - end

    # Stock remove watchlist - start
    Scenario: remove invalid stock from watchlist
        Given path '' + 34424242
        Given method Delete
        And status 500
        And match response.errors[0] contains '34424242 exists'

    # Stock remove watchlist - end