package org.blackmagic.usersservice.exception.handler;

import org.blackmagic.usersservice.exception.UsersCustomErrorResponse;
import org.blackmagic.usersservice.exception.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author niharsh
 * @created 21/06/2021 - 12:56
 */
@ControllerAdvice
public class UserExceptionHandler extends ResponseEntityExceptionHandler {

    Logger logger = LoggerFactory.getLogger(UserExceptionHandler.class);

    @ExceptionHandler({UserNotFoundException.class})
    protected ResponseEntity<Object> handleNotFound(Exception ex, WebRequest req){
        logger.error(ex.getMessage());
        UsersCustomErrorResponse errorResponse = createResponse(ex, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({DataIntegrityViolationException.class})
    protected ResponseEntity<UsersCustomErrorResponse> handleDuplicateInsertion(Exception ex, WebRequest req){
        ex.printStackTrace();
        ResponseEntity responseEntity = null;
        if(ex.getCause() != null && ex.getCause().getCause() !=null){
            String message = ex.getCause().getCause().getMessage();
            UsersCustomErrorResponse errorResponse;
            if(message.contains("Unique index or primary key violation")){
                errorResponse = createResponse(ex, HttpStatus.CONFLICT);
                errorResponse.setErrors(Arrays.asList("Account already exist"));
                responseEntity = new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
            }else{
                errorResponse = createResponse(ex, HttpStatus.INTERNAL_SERVER_ERROR);
                responseEntity = new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return responseEntity;
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", status.value());

        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());

        logger.error("Invalid request received - {}", errors);
        body.put("errors", errors);

        return new ResponseEntity<>(body, headers, status);

    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> globalExceptionHandler(Exception ex, WebRequest request) {
        ex.printStackTrace();
        UsersCustomErrorResponse errorResponse = createResponse(ex, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private UsersCustomErrorResponse createResponse(Exception ex, HttpStatus status) {
        UsersCustomErrorResponse errorResponse = new UsersCustomErrorResponse();
        errorResponse.setTimestamp(LocalDateTime.now());
        errorResponse.setStatus(status.value());
        errorResponse.setErrors(Arrays.asList(ex.getMessage()));
        return errorResponse;
    }

}
