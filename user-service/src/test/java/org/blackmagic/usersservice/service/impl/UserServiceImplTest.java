package org.blackmagic.usersservice.service.impl;

import org.blackmagic.usersservice.entity.UserEntity;
import org.blackmagic.usersservice.exception.UserNotFoundException;
import org.blackmagic.usersservice.mapper.UserMapper;
import org.blackmagic.usersservice.modal.RegistrationUser;
import org.blackmagic.usersservice.modal.User;
import org.blackmagic.usersservice.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author niharsh
 * @created 27/06/2021 - 11:07
 */
@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserMapper userMapper;

    @Autowired
    @InjectMocks
    private UserServiceImpl userService;


    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void givenUserThenShouldBeAbleToSave(){
        RegistrationUser registerUser = new RegistrationUser();
        registerUser.setEmail("registeruser@gmail.com");
        registerUser.setPassword("register");
        registerUser.setDob(new Date());

        UserEntity userEntity = new UserEntity();
        userEntity.setId( registerUser.getId() );
        userEntity.setEmail( registerUser.getEmail() );
        userEntity.setPassword( registerUser.getPassword() );
        userEntity.setDob( registerUser.getDob() );

        when(userMapper.userToUserEntity(registerUser)).thenReturn(userEntity);
        when(userMapper.userEntityToUser(userEntity)).thenReturn(registerUser);
        when(userRepository.save(any())).thenReturn(userEntity);
        userService.saveUser(registerUser);
        verify(userRepository,times(1)).save(any());
    }

    @Test()
    void givenNullThenShouldThrowNPE(){
        assertThrows(NullPointerException.class, ()->{
            userService.saveUser(null);
        });
    }

    @Test
    void givenValidUserThenShouldBeAbleToAuthenticate(){
        User user = new User();
        user.setEmail("authenticate@gmail.com");
        user.setPassword("password");

        UserEntity userEntity = new UserEntity();
        userEntity.setId(1);
        userEntity.setEmail( user.getEmail() );
        userEntity.setPassword( user.getPassword() );

        when(userRepository.findByEmail(any())).thenReturn(Optional.of(userEntity));
        long isValid = userService.authenticate(user);
        assertEquals(1, isValid);
    }

    @Test
    void givenInValidPasswordThenShouldAuthenticationFail(){
        User user = new User();
        user.setEmail("authenticate@gmail.com");
        user.setPassword("invalidpassword");

        UserEntity userEntity = new UserEntity();
        userEntity.setId(1);
        userEntity.setEmail( user.getEmail() );
        userEntity.setPassword("validpassword");

        when(userRepository.findByEmail(any())).thenReturn(Optional.of(userEntity));
        long isValid = userService.authenticate(user);
        assertEquals(-1, isValid);
    }

    @Test
    void givenNonExistUserThenShouldThrowUNFExp(){
        User user = new User();
        user.setEmail("authenticate@gmail.com");
        user.setPassword("password");
        when(userRepository.findByEmail(any())).thenReturn(Optional.empty());
        assertThrows(UserNotFoundException.class, ()->{
            userService.authenticate(user);
        });
    }

}