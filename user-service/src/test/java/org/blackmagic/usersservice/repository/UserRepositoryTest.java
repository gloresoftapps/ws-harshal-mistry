package org.blackmagic.usersservice.repository;

import org.blackmagic.usersservice.entity.UserEntity;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author niharsh
 * @created 27/06/2021 - 09:11
 */
@ExtendWith(SpringExtension.class)
@DataJpaTest
class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    private UserEntity userEntity;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
        userRepository.deleteAll();
    }

    @Test
    void givenUserThenShouldBeAbleToSave(){
        userEntity = new UserEntity(1, "test@gmail.com", "test", new Date());
        UserEntity save = userRepository.save(userEntity);
        UserEntity fetchedUserEntity = userRepository.findById(save.getId()).get();
        assertEquals(userEntity.getEmail(), fetchedUserEntity.getEmail());
    }

    @Test
    void givenValidEmailThenShouldReturnUser() {
        String email = "randomemail@gmail.com";
        userEntity = new UserEntity(2, email, "test", new Date());
        userRepository.save(userEntity);
        Optional<UserEntity> byEmail = userRepository.findByEmail(email);
        UserEntity userEntity = byEmail.get();
        assertEquals(email, userEntity.getEmail());
    }

    @Test
    void givenNonExistEmailThenShouldReturnNoUser() {
        String nonExistingUserEmail = "idontexist@gmail.com";
        Optional<UserEntity> byEmail = userRepository.findByEmail(nonExistingUserEmail);
        assertEquals(Optional.empty(), byEmail);
    }

    @Test
    void getAllUsers(){
        UserEntity ue1 = new UserEntity(11, "getall1@gmail.com", "test1", new Date());
        UserEntity ue2 = new UserEntity(12, "getall2@gmail.com", "test2", new Date());
        userRepository.save(ue1);
        userRepository.save(ue2);
        List<UserEntity> all = (List<UserEntity>)userRepository.findAll();
        assertEquals("getall1@gmail.com", all.get(0).getEmail());
    }

    @Test
    void deleteById(){
        UserEntity ue = new UserEntity(20, "delete@gmail.com", "delete", new Date());
        UserEntity save = userRepository.save(ue);
        userRepository.deleteById(save.getId());
        Optional<UserEntity> byId = userRepository.findById(save.getId());
        assertEquals(Optional.empty(), byId);
    }
}