package org.blackmagic.usersservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.blackmagic.usersservice.modal.RegistrationUser;
import org.blackmagic.usersservice.modal.User;
import org.blackmagic.usersservice.service.UserService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.http.MediaType;

/**
 * @author niharsh
 * @created 27/06/2021 - 12:11
 */
@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;
    private RegistrationUser registrationUser;
    private User user;
    private final static String REGISTER = "/api/users/register";
    private final static String AUTHENTICATE = "/api/users/authenticate";

    @BeforeEach
    void setUp() {
        registrationUser = new RegistrationUser();
        registrationUser.setId(1);
        registrationUser.setEmail("controller@gmail.com");
        registrationUser.setPassword("password");
        registrationUser.setDob(new Date());
        user = new User();
        user.setId(registrationUser.getId());
        user.setEmail(registrationUser.getEmail());
        user.setPassword(registrationUser.getPassword());
        lenient().when(userService.saveUser(registrationUser)).thenReturn(user);
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @AfterEach
    void tearDown() {
        registrationUser = null;
        user = null;
    }

    @Test
    void loginUserSuccess() throws Exception {
        mockMvc.perform(post(AUTHENTICATE).
                contentType(MediaType.APPLICATION_JSON).
                content(asJsonString(user))).
                andExpect(status().isOk());
        verify(userService,times(1)).authenticate(any());
    }

    @Test
    void loginUserEmptyUser() throws Exception {
        user.setEmail("");
        user.setPassword("");
        mockMvc.perform(post(AUTHENTICATE).
                contentType(MediaType.APPLICATION_JSON).
                content(asJsonString(user))).
                andExpect(status().isBadRequest());
        verify(userService,times(0)).authenticate(any());
    }

    @Test
    void registerUserSuccess() throws Exception {
        mockMvc.perform(post(REGISTER).
                contentType(MediaType.APPLICATION_JSON).
                content(asJsonString(registrationUser))).
                andExpect(status().isCreated());
        verify(userService,times(1)).saveUser(any());
    }

    @Test
    void registerUserMissingDOB() throws Exception {
        registrationUser.setDob(null);
        mockMvc.perform(post(REGISTER).
                contentType(MediaType.APPLICATION_JSON).
                content(asJsonString(registrationUser))).
                andExpect(status().is4xxClientError());
        verify(userService,times(0)).saveUser(any());
    }

    @Test
    void registerUserMissingPassword() throws Exception {
        registrationUser.setPassword(null);
        mockMvc.perform(post(REGISTER).
                contentType(MediaType.APPLICATION_JSON).
                content(asJsonString(registrationUser))).
                andExpect(status().is4xxClientError());
        verify(userService,times(0)).saveUser(any());
    }

    @Test
    void registerUserMissingEmail() throws Exception {
        registrationUser.setEmail(null);
        mockMvc.perform(post(REGISTER).
                contentType(MediaType.APPLICATION_JSON).
                content(asJsonString(registrationUser))).
                andExpect(status().is4xxClientError());
        verify(userService,times(0)).saveUser(any());
    }

    @Test
    void registerUserAllEmptyFields() throws Exception {
        registrationUser.setEmail("");
        registrationUser.setPassword("");
        mockMvc.perform(post(REGISTER).
                contentType(MediaType.APPLICATION_JSON).
                content(asJsonString(registrationUser))).
                andExpect(status().is4xxClientError());
        verify(userService,times(0)).saveUser(any());
    }

    @Test
    void registerUserPasswordLength() throws Exception {
        registrationUser.setPassword("12345");
        mockMvc.perform(post(REGISTER).
                contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(registrationUser)))
                .andExpect(status().is4xxClientError());
        verify(userService,times(0)).saveUser(any());
    }

    public static String asJsonString(final Object obj){
        try{
            return new ObjectMapper().writeValueAsString(obj);
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

}