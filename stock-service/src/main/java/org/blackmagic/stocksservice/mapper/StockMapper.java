package org.blackmagic.stocksservice.mapper;

import org.blackmagic.stocksservice.entity.StockEntity;
import org.blackmagic.stocksservice.model.Stock;
import org.blackmagic.stocksservice.model.WatchListStock;
import org.mapstruct.Mapper;

/**
 * @author niharsh
 * @created 25/06/2021 - 10:51
 */
@Mapper(componentModel = "spring")
public interface StockMapper  {

    default StockEntity stockToStockEntity(Stock stock){
        if(stock instanceof WatchListStock){
            return map((WatchListStock)stock);
        } else{
            return map(stock);
        }
    }

    StockEntity map(Stock stock);

    StockEntity map(WatchListStock stock);

    WatchListStock stockEntityToWatchListStock(WatchListStock stock);
}
