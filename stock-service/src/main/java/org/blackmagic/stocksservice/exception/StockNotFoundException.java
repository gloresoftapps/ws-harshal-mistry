package org.blackmagic.stocksservice.exception;

/**
 * @author niharsh
 * @created 21/06/2021 - 16:09
 */
public class StockNotFoundException extends RuntimeException{
    public StockNotFoundException(String stockName) {
        super("No stock found for - "+stockName);
    }
}
