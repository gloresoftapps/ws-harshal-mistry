package org.blackmagic.stocksservice.exception;

/**
 * @author niharsh
 * @created 21/06/2021 - 16:13
 */
public class StockTechnicalException extends RuntimeException {
    public StockTechnicalException(String message) {
        super(message);
    }
}
