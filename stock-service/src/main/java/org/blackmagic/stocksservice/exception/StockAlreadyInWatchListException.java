package org.blackmagic.stocksservice.exception;

/**
 * @author niharsh
 * @created 25/06/2021 - 14:10
 */
public class StockAlreadyInWatchListException extends RuntimeException {
    public StockAlreadyInWatchListException(String stockName, long userId) {
        super("Stock - " + stockName + " already in watchlist for - " + userId);
    }
}
