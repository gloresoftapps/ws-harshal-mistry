package org.blackmagic.stocksservice.exception.handler;

import org.blackmagic.stocksservice.exception.StockAlreadyInWatchListException;
import org.blackmagic.stocksservice.exception.StockNotFoundException;
import org.blackmagic.stocksservice.exception.StockTechnicalException;
import org.blackmagic.stocksservice.exception.StocksCustomErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author niharsh
 * @created 21/06/2021 - 16:10
 */
@ControllerAdvice
public class StockExceptionHandler extends ResponseEntityExceptionHandler {

    Logger logger = LoggerFactory.getLogger(StockExceptionHandler.class);

    @ExceptionHandler({StockNotFoundException.class})
    protected ResponseEntity<StocksCustomErrorResponse> handleNotFound(Exception ex, WebRequest req){
        logger.error(ex.getMessage());
        StocksCustomErrorResponse errorResponse = createResponse(ex, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({StockAlreadyInWatchListException.class})
    protected ResponseEntity<StocksCustomErrorResponse> handleDuplicate(Exception ex, WebRequest req){
        logger.error(ex.getMessage());
        StocksCustomErrorResponse errorResponse = createResponse(ex, HttpStatus.CONFLICT);
        return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
    }


    private StocksCustomErrorResponse createResponse(Exception ex, HttpStatus status) {
        StocksCustomErrorResponse errorResponse = new StocksCustomErrorResponse();
        errorResponse.setTimestamp(LocalDateTime.now());
        errorResponse.setStatus(status.value());
        errorResponse.setErrors(Arrays.asList(ex.getMessage()));
        return errorResponse;
    }

    @ExceptionHandler({ConstraintViolationException.class})
    protected ResponseEntity<StocksCustomErrorResponse> handleBadRequest(Exception ex, WebRequest req){
        logger.error(ex.getMessage());
        StocksCustomErrorResponse errorResponse = createResponse(ex, HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler({StockTechnicalException.class})
    protected ResponseEntity<Object> handleTechnicalException(Exception ex, WebRequest req){
        ex.printStackTrace();
        logger.error(ex.getMessage());
        StocksCustomErrorResponse errorResponse = createResponse(ex, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", status.value());

        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());

        logger.error("Invalid request received - {}", errors);
        body.put("errors", errors);

        return new ResponseEntity<>(body, headers, status);

    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> globalExceptionHandler(Exception ex, WebRequest request) {
        StocksCustomErrorResponse errorResponse = createResponse(ex, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
