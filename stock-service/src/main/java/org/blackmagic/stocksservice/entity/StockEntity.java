package org.blackmagic.stocksservice.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

/**
 * @author niharsh
 * @created 20/06/2021 - 11:09
 */
@Entity
@Table(name = "stock")
@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
@ToString
public class StockEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(nullable = false)
    private long userId;
    private String userNote;
    @NotEmpty
    @Column(nullable = false)
    private String stockName;
}
