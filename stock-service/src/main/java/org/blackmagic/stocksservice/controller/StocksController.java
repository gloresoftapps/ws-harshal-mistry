package org.blackmagic.stocksservice.controller;

import org.blackmagic.stocksservice.model.WatchListStock;
import org.blackmagic.stocksservice.model.Stock;
import org.blackmagic.stocksservice.service.StockService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author niharsh
 * @created 19/06/2021 - 11:31
 */
@RestController
@RequestMapping("/api/stocks")
@Validated
public class StocksController {

    Logger logger = LoggerFactory.getLogger(StocksController.class);

    @Autowired
    private StockService stockService;

    @GetMapping("/{stockName}")
    public Stock getStockByName(@PathVariable @NotEmpty(message = "Please provide stock name") String stockName){
        logger.debug("searching stock - {}", stockName);
        return stockService.findStockByName(stockName);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public long addStockToWatchList(@Valid @RequestBody WatchListStock watchListStock){
        logger.debug("adding stock - {} to watchlist for user - {}", watchListStock.getStockName(), watchListStock.getUserId());
        return stockService.addStock(watchListStock);
    }

    @GetMapping
    public List<Stock> getStocksByUserId(
            @Min(value = 1, message = "Please provide valid userId")
            @RequestParam(value = "userId") long userId){
        logger.debug("fetching stocks for user - {}", userId);
        return stockService.getStocksByUserId(userId);
    }

    @DeleteMapping("/{id}")
    public void removeStockFromWatchList(@PathVariable @Min(value = 1, message = "Please provide valid id") long id){
        logger.debug("removing stock - {} from watchlist", id);
        stockService.removeStock(id);
    }
    
    @PutMapping("/{id}")
    public long updateWatchListStock(@Valid @RequestBody WatchListStock watchListStock, @PathVariable @Min(value = 1, message = "Please provide valid id") long id){
        logger.debug("updating stock - {} to watchlist for user - {}", watchListStock.getStockName(), watchListStock.getUserId());
        watchListStock.setId(id);
        return stockService.updateStock(watchListStock);
    }

}
