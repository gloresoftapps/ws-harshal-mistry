package org.blackmagic.stocksservice.repository;

import org.blackmagic.stocksservice.entity.StockEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author niharsh
 * @created 20/06/2021 - 11:13
 */
@Repository
public interface StockRepository extends CrudRepository<StockEntity, Long> {
    List<StockEntity> findAllByUserId(long userId);
    Optional<StockEntity> findByStockNameAndUserId(String stockName, long userId);
}
