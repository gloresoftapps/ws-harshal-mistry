package org.blackmagic.stocksservice.service;
import org.blackmagic.stocksservice.model.Stock;

import java.util.List;

/**
 * @author niharsh
 * @created 18/06/2021 - 16:37
 */
public interface StockService {

    Stock findStockByName(final String stockName);
    long addStock(Stock stock);
    long updateStock(Stock stock);
    List<Stock> getStocksByUserId(long userId);
    void removeStock(long id);

}
