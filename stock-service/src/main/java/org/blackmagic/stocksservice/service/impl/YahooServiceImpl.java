package org.blackmagic.stocksservice.service.impl;

import lombok.AllArgsConstructor;
import org.blackmagic.stocksservice.service.YahooService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;

import java.io.IOException;
import java.util.Optional;

/**
 * @author niharsh
 * @created 20/06/2021 - 10:23
 */
@Service
public class YahooServiceImpl implements YahooService {

    Logger logger = LoggerFactory.getLogger(YahooServiceImpl.class);

    @Override
    public Optional<Stock> findStock(String stockName) {
        Stock stock = null;
        try{
            stock = YahooFinance.get(stockName);
        }
        catch (IOException e){
            logger.error("Exception while processing find stock for - {}", stockName);
        }
        return Optional.ofNullable(stock);
    }
}
