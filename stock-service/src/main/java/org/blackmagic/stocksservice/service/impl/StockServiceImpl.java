package org.blackmagic.stocksservice.service.impl;

import org.blackmagic.stocksservice.entity.StockEntity;
import org.blackmagic.stocksservice.exception.StockAlreadyInWatchListException;
import org.blackmagic.stocksservice.exception.StockNotFoundException;
import org.blackmagic.stocksservice.exception.StockTechnicalException;
import org.blackmagic.stocksservice.mapper.StockMapper;
import org.blackmagic.stocksservice.model.FindStock;
import org.blackmagic.stocksservice.model.WatchListStock;
import org.blackmagic.stocksservice.model.Stock;
import org.blackmagic.stocksservice.repository.StockRepository;
import org.blackmagic.stocksservice.service.StockService;
import org.blackmagic.stocksservice.service.YahooService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;
import yahoofinance.quotes.stock.StockQuote;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author niharsh
 * @created 20/06/2021 - 10:17
 */
@Service
public class StockServiceImpl implements StockService {

    Logger logger = LoggerFactory.getLogger(StockServiceImpl.class);

    @Autowired
    private YahooService yahooService;

    @Autowired
    private StockMapper stockMapper;

    @Autowired
    private StockRepository stockRepository;

    public FindStock findStockByName(final String stockName){
        Objects.requireNonNull(stockName);
        Optional<yahoofinance.Stock> optionalStock = yahooService.findStock(stockName);
        optionalStock.orElseThrow(()-> new StockNotFoundException(stockName));
        FindStock resultStock = new FindStock();
        resultStock.setStockName(stockName);
        optionalStock.ifPresent(stock -> {
            try {
                StockQuote stockQuote = optionalStock.get().getQuote(false);
                resultStock.setPrice(stockQuote.getPrice());
                resultStock.setYearHigh(stockQuote.getYearHigh());
                resultStock.setYearLow(stockQuote.getYearLow());
                resultStock.setOpen(stockQuote.getOpen());
                resultStock.setPreviousClose(stockQuote.getPreviousClose());
            } catch (IOException e) {
                logger.error("Error getting stock details = {}", e.getCause());
                throw new StockTechnicalException(e.getMessage());
            }
        });
        logger.debug("for stock name - {} returning stock - {}", stockName, resultStock);
        return resultStock;
    }

    @Override
    public long addStock(Stock stock) {
        Objects.requireNonNull(stock);
        Optional<StockEntity> byStockNameAndUserId = stockRepository.findByStockNameAndUserId(stock.getStockName(), stock.getUserId());
        byStockNameAndUserId.ifPresent((s) -> {
            throw new StockAlreadyInWatchListException(stock.getStockName(), stock.getUserId());
        });
        StockEntity stockEntity = doAddOrUpdate(stock);
        logger.info("stock added to watchlist - {} - {} - {}", stockEntity.getId(), stockEntity.getUserId(), stockEntity.getStockName());
        return stockEntity.getId();
    }

    private StockEntity doAddOrUpdate(Stock stock){
        StockEntity stockEntity = stockMapper.stockToStockEntity(stock);
        logger.debug("stock mapped {} -> {}", stockEntity);
        StockEntity save = stockRepository.save(stockEntity);
        return save;
    }

    @Override
    public long updateStock(Stock stock) {
        Objects.requireNonNull(stock);
        StockEntity stockEntity = doAddOrUpdate(stock);
        logger.info("stock updated to watchlist - {} - {} - {}", stockEntity.getId(), stockEntity.getUserId(), stockEntity.getStockName());
        return stockEntity.getId();
    }

    @Override
    public List<Stock> getStocksByUserId(long userId) {
        // fetching watchlist stocks for user from DB
        List<StockEntity> allByUserId = stockRepository.findAllByUserId(userId);

        // getting only stocks name to query yahoo finance api
        List<String> stocks = allByUserId.parallelStream().map(StockEntity::getStockName).collect(Collectors.toList());
        logger.info("watchlist stocks - {} for user - {}", stocks.toString(), userId);

        // calculate gain/lose for watchlist stocks
        Map<String, WatchListStock> watchListStockMap = calculateGain(stocks);

        // mapping entity to presentational
        List<Stock> watchListStockList = allByUserId.parallelStream().map(stock -> {
            WatchListStock watchListStock = watchListStockMap.get(stock.getStockName());
            watchListStock.setStockName(stock.getStockName());
            watchListStock.setUserId(stock.getUserId());
            watchListStock.setUserNote(stock.getUserNote());
            watchListStock.setId(stock.getId());
            return watchListStock;
        }).collect(Collectors.toList());
        logger.info("returning - {} - watchlist stocks for user - {}", watchListStockList.size(), userId);
        return watchListStockList;
    }

    @Override
    public void removeStock(long id) {
        stockRepository.deleteById(id);
    }

    private Map<String, WatchListStock> calculateGain(List<String> stocks) {
        Map<String, WatchListStock> finalMap = new HashMap<>();
        if(stocks.isEmpty()){
            logger.debug("empty watchlist stocks received, returning empty map");
            return finalMap;
        }

        // fetching 1 month historical data for list of stocks
        Map<String, yahoofinance.Stock> stocksMap = getHistoricalStocks(stocks);

        // applying calculation for profit/lose
        stocksMap.entrySet().parallelStream().forEach((entry)->{
            WatchListStock watchListStock = new WatchListStock();
            yahoofinance.Stock value = entry.getValue();
            List<HistoricalQuote> history = null;
            try {
                history = value.getHistory();

                // getting stocks current details
                StockQuote quote = value.getQuote(false);
                BigDecimal price = quote.getPrice();
                BigDecimal yesterdayClose = quote.getPreviousClose();

                // calculating today's gain
                int todayGain = calculatePercentage(price, yesterdayClose);
                watchListStock.setCurrent(price);
                watchListStock.setTodayGain(todayGain);
            } catch (IOException e) {
                throw new StockTechnicalException("Error while fetching stocks details");
            }

            int size = history.size();
            // calculating last week's gain
            BigDecimal lastWeekClose = getStockClosePriceBeforeDays(size-6, history);
            int lastWeekGain = calculatePercentage(watchListStock.getCurrent(), lastWeekClose);
            watchListStock.setLastWeekGain(lastWeekGain);

            // calculating last 2 week's gain
            BigDecimal last2WeekClose = getStockClosePriceBeforeDays(size-11, history);
            int last2WeekGain = calculatePercentage(watchListStock.getCurrent(), last2WeekClose);
            watchListStock.setTwoWeeksGain(last2WeekGain);

            // calculating last 3 week's gain
            BigDecimal last3WeekClose = getStockClosePriceBeforeDays(size-16, history);
            int last3WeekGain = calculatePercentage(watchListStock.getCurrent(), last3WeekClose);
            watchListStock.setThreeWeeksGain(last3WeekGain);

            // calculating last month's gain
            BigDecimal monthBackClose = getStockClosePriceBeforeDays(0, history);
            int monthBackGain = calculatePercentage(watchListStock.getCurrent(), monthBackClose);
            watchListStock.setMonthGain(monthBackGain);

            finalMap.put(entry.getKey(), watchListStock);
        });
        return finalMap;
    }

    private Map<String, yahoofinance.Stock> getHistoricalStocks(List<String> stocks) {
        // fetching 1 month historical data for list of stocks
        Calendar from = Calendar.getInstance();
        Calendar to = Calendar.getInstance();
        from.add(Calendar.MONTH, -1);
        try {
            return YahooFinance.get(stocks.toArray(new String[]{}), from, to, Interval.DAILY);
        }catch (IOException ex){
            throw new StockTechnicalException("Error while fetching stocks historical data");
        }
    }

    private BigDecimal getStockClosePriceBeforeDays(int days, List<HistoricalQuote> history){
        HistoricalQuote historicalQuote = history.get(days);
        return historicalQuote.getClose();
    }

    private int calculatePercentage(BigDecimal current, BigDecimal yesterdayClose) {
        BigDecimal gain = current.subtract(yesterdayClose).divide(yesterdayClose, 2, RoundingMode.HALF_UP ).multiply(new BigDecimal(100));
        return gain.intValue();
    }
}
