package org.blackmagic.stocksservice.service;

import yahoofinance.Stock;

import java.util.Optional;

/**
 * @author niharsh
 * @created 20/06/2021 - 10:20
 */
public interface YahooService {
    Optional<Stock> findStock(final String stockName);
}
