package org.blackmagic.stocksservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

/**
 * @author niharsh
 * @created 20/06/2021 - 18:53
 */
@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Stock {
    private long id;

    // userId will be always 1 or greater
    @Min(value = 1, message = "Please provide valid userId")
    private long userId;

    @NotEmpty(message = "Please provide stockName")
    private String stockName;
}
