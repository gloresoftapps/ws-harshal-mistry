package org.blackmagic.stocksservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author niharsh
 * @created 18/06/2021 - 17:02
 */

@Getter
@Setter
@AllArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class FindStock extends Stock {
    private LocalDateTime lastAccessTime;
    private BigDecimal yearHigh;
    private BigDecimal price;
    private BigDecimal yearLow;
    private BigDecimal open;
    private BigDecimal previousClose;

    public FindStock() {
        this.lastAccessTime = LocalDateTime.now();
    }
}
