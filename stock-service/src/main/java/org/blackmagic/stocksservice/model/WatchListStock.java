package org.blackmagic.stocksservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.math.BigDecimal;

/**
 * @author niharsh
 * @created 20/06/2021 - 11:20
 */
@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class WatchListStock extends Stock {
    private String userNote;
    private BigDecimal current;
    private int todayGain;
    private int lastWeekGain;
    private int twoWeeksGain;
    private int threeWeeksGain;
    private int monthGain;
}
