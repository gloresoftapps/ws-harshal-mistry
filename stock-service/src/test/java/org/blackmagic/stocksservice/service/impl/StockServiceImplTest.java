package org.blackmagic.stocksservice.service.impl;

import org.blackmagic.stocksservice.entity.StockEntity;
import org.blackmagic.stocksservice.exception.StockAlreadyInWatchListException;
import org.blackmagic.stocksservice.exception.StockNotFoundException;
import org.blackmagic.stocksservice.mapper.StockMapper;
import org.blackmagic.stocksservice.model.FindStock;
import org.blackmagic.stocksservice.repository.StockRepository;
import org.blackmagic.stocksservice.service.YahooService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.blackmagic.stocksservice.model.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;
import yahoofinance.quotes.stock.StockQuote;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
/**
 * @author niharsh
 * @created 27/06/2021 - 15:17
 */
@ExtendWith(MockitoExtension.class)
class StockServiceImplTest {

    @Mock
    private YahooService yahooService;
    @Mock
    private StockMapper stockMapper;
    @Mock
    private StockRepository stockRepository;

    @Autowired
    @InjectMocks
    private StockServiceImpl stockService;

    private static final String IDEANS = "IDEA.NS";
    private static final String RCOMNS = "RCOM.NS";

    @Test
    void find_stock_by_name_null_stock_name() {
        assertThrows(NullPointerException.class, ()->{
            stockService.findStockByName(null);
        });
    }

    @Test
    void find_stock_by_name_non_exist_stock_name() {
        assertThrows(StockNotFoundException.class, ()->{
            stockService.findStockByName("XYZIOP");
        });
    }

    @Test
    void find_stock_by_name_valid_stock_name() throws IOException {
        yahoofinance.Stock yahooStock = getYahooStock(IDEANS);
        when(yahooService.findStock(IDEANS)).thenReturn(Optional.of(yahooStock));
        FindStock stockByName = stockService.findStockByName(IDEANS);
        assertEquals(yahooStock.getQuote(false).getPrice(), stockByName.getPrice());
        assertEquals(yahooStock.getQuote(false).getYearHigh(), stockByName.getYearHigh());
        assertEquals(yahooStock.getQuote(false).getYearLow(), stockByName.getYearLow());
        assertEquals(yahooStock.getQuote(false).getOpen(), stockByName.getOpen());
        assertEquals(yahooStock.getQuote(false).getPreviousClose(), stockByName.getPreviousClose());
    }

    @Test
    void add_stock_null_stock(){
        assertThrows(NullPointerException.class, ()->{
            stockService.addStock(null);
        });
    }

    @Test
    void add_stock_stock_already_present(){
        Stock stock = new Stock();
        stock.setStockName(IDEANS);
        stock.setUserId(1);
        StockEntity stockEntity = new StockEntity();
        stockEntity.setId(1);
        stockEntity.setStockName(IDEANS);
        stockEntity.setUserId(1);
        when(stockRepository.findByStockNameAndUserId(stock.getStockName(), stock.getUserId())).thenReturn(Optional.of(stockEntity));
        assertThrows(StockAlreadyInWatchListException.class, ()->{
            stockService.addStock(stock);
        });
    }

    @Test
    void add_stock_stock_success(){
        Stock stock = new Stock();
        stock.setStockName(IDEANS);
        stock.setUserId(1);
        StockEntity stockEntity = new StockEntity(1, 1, "", IDEANS);
        when(stockRepository.findByStockNameAndUserId(stock.getStockName(), stock.getUserId())).thenReturn(Optional.empty());
        when(stockMapper.stockToStockEntity(stock)).thenReturn(stockEntity);
        when(stockRepository.save(stockEntity)).thenReturn(stockEntity);
        long id = stockService.addStock(stock);
        assertEquals(1, id);
    }

    @Test
    void remove_stock(){
        stockService.removeStock(1);
        verify(stockRepository,times(1)).deleteById(any());
    }

//    @Test
//    void get_stocks_by_user_id() {
//        when(stockRepository.findAllByUserId(1)).thenReturn(getStockEntities());
//        try (MockedStatic mocked = mockStatic(YahooFinance.class)) {
//            Map<String, yahoofinance.Stock> map = new HashMap<>();
//            yahoofinance.Stock idea = getYahooStock(IDEANS);
//            yahoofinance.Stock rcom = getYahooStock(RCOMNS);
//            map.put(IDEANS, idea); map.put(RCOMNS, rcom);
//            Calendar from = Calendar.getInstance();
//            Calendar to = Calendar.getInstance();
//            from.add(Calendar.MONTH, -1);
//            mocked.when(() -> YahooFinance.get(any(String[].class) , any(Calendar.class), any(Calendar.class), any(Interval.class))).thenReturn(map);
//            assertEquals(2, YahooFinance.get(new String[]{IDEANS, RCOMNS} , from, to, Interval.DAILY).size());
//            List<Stock> stocksByUserId = stockService.getStocksByUserId(1);
//            //assertEquals(2, stocksByUserId.size());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }


    private yahoofinance.Stock getYahooStock(String stockName){
        yahoofinance.Stock yahooStock = new yahoofinance.Stock(stockName);
        StockQuote stockQuote = new StockQuote(stockName);
        stockQuote.setPrice(new BigDecimal(1));
        stockQuote.setYearHigh(new BigDecimal(1));
        stockQuote.setYearLow(new BigDecimal(1));
        stockQuote.setOpen(new BigDecimal(1));
        stockQuote.setPreviousClose(new BigDecimal(1));
        yahooStock.setQuote(stockQuote);
        return yahooStock;
    }

    private List<StockEntity> getStockEntities(){
        List<StockEntity> list = new ArrayList<>();
        StockEntity stockEntity1 = new StockEntity(1, 1, "", IDEANS);
        StockEntity stockEntity2 = new StockEntity(2, 1, "", RCOMNS);
        list.add(stockEntity1); list.add(stockEntity2);
        return list;
    }
}