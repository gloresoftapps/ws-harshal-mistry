package org.blackmagic.stocksservice.repository;

import org.blackmagic.stocksservice.entity.StockEntity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author niharsh
 * @created 27/06/2021 - 14:54
 */
@ExtendWith(SpringExtension.class)
@DataJpaTest
class StockRepositoryTest {

    @Autowired
    private StockRepository stockRepository;

    @BeforeEach
    void setUp() {

    }

    @Test
    void save_stock_entity_success(){
        StockEntity stockEntity = new StockEntity(1000000, 1, "", "IDEA.NS");
        StockEntity save = stockRepository.save(stockEntity);
        Optional<StockEntity> byId = stockRepository.findById(save.getId());
        assertEquals(stockEntity.getStockName(), byId.get().getStockName());
    }

    @Test
    void find_all_by_user_id_success(){
        StockEntity se1 = new StockEntity(1, 1, "", "IDEA.NS");
        StockEntity se2 = new StockEntity(2, 1, "", "RCOM.NS");
        stockRepository.save(se1);
        stockRepository.save(se2);
        List<StockEntity> allByUserId = stockRepository.findAllByUserId(1);
        assertEquals(2, allByUserId.size());
    }

    @Test
    void find_all_by_user_id_non_exist_user_id(){
        List<StockEntity> allByUserId = stockRepository.findAllByUserId(0);
        assertEquals(0, allByUserId.size());
    }

    @Test
    void find_by_stock_name_and_user_id_success(){
        StockEntity stockEntity = new StockEntity(1, 1, "", "IDEA.NS");
        stockRepository.save(stockEntity);
        Optional<StockEntity> byStockNameAndUserId = stockRepository.findByStockNameAndUserId(stockEntity.getStockName(), stockEntity.getUserId());
        assertEquals(stockEntity.getStockName(), byStockNameAndUserId.get().getStockName());
    }

    @Test
    void find_by_stock_name_and_user_id_negative(){
        Optional<StockEntity> byStockNameAndUserId = stockRepository.findByStockNameAndUserId("ABCDEF.NS", 100);
        assertEquals(Optional.empty(), byStockNameAndUserId);
    }

    @AfterEach
    void tearDown() {
        stockRepository.deleteAll();
    }
}