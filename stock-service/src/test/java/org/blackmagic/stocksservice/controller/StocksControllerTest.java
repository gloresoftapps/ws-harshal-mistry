package org.blackmagic.stocksservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.blackmagic.stocksservice.model.FindStock;
import org.blackmagic.stocksservice.model.WatchListStock;
import org.blackmagic.stocksservice.service.StockService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.ws.rs.core.MediaType;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author niharsh
 * @created 27/06/2021 - 17:22
 */
@ExtendWith(MockitoExtension.class)
class StocksControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private StockService stockService;

    @InjectMocks
    private StocksController stocksController;
    private final static String BASE_URL = "/api/stocks";
    private static final String IDEANS = "IDEA.NS";

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(stocksController).build();
    }

    @Test
    void get_stock_by_name() throws Exception {
        FindStock stock = new FindStock();
        stock.setStockName(IDEANS);
        stock.setPrice(new BigDecimal(1));
        lenient().when(stockService.findStockByName(IDEANS)).thenReturn(stock);
        mockMvc.perform(get(BASE_URL + "/" + IDEANS)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        verify(stockService,times(1)).findStockByName(any());
    }

    @Test
    void get_stock_by_name_empty_name() throws Exception {

        mockMvc.perform(get(BASE_URL + "/")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
        verify(stockService,times(0)).findStockByName(any());
    }

    @Test
    void add_stock_to_watch_list_success() throws Exception {
        WatchListStock watchListStock = new WatchListStock();
        watchListStock.setId(1); watchListStock.setStockName(IDEANS);
        watchListStock.setUserId(1);
        mockMvc.perform(post(BASE_URL).
                contentType(org.springframework.http.MediaType.APPLICATION_JSON).
                content(asJsonString(watchListStock))).
                andExpect(status().isCreated());
        verify(stockService,times(1)).addStock(any());
    }

    @Test
    void add_stock_to_watch_list_empty_values() throws Exception {
        WatchListStock watchListStock = new WatchListStock();
        mockMvc.perform(post(BASE_URL).
                contentType(org.springframework.http.MediaType.APPLICATION_JSON).
                content(asJsonString(watchListStock))).
                andExpect(status().isBadRequest());
        verify(stockService,times(0)).addStock(any());
    }

    @Test
    void get_stocks_by_user_id() throws Exception {
        when(stockService.getStocksByUserId(1)).thenReturn(anyList());
        mockMvc.perform(get(BASE_URL)
                .param("userId", String.valueOf(1))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void remove_stock_from_watch_list() throws Exception{
        mockMvc.perform(delete(BASE_URL + "/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        verify(stockService,times(1)).removeStock(1);
    }

    @Test
    void update_watch_list_stock() throws Exception{
        WatchListStock watchListStock = new WatchListStock();
        watchListStock.setId(1); watchListStock.setStockName(IDEANS);
        watchListStock.setUserId(1);
        mockMvc.perform(put(BASE_URL + "/1").
                contentType(org.springframework.http.MediaType.APPLICATION_JSON).
                content(asJsonString(watchListStock))).
                andExpect(status().isOk());
        verify(stockService,times(1)).updateStock(any());
    }

    @Test
    void update_watch_list_stock_empty() throws Exception{
        WatchListStock watchListStock = new WatchListStock();
        mockMvc.perform(put(BASE_URL + "/1").
                contentType(org.springframework.http.MediaType.APPLICATION_JSON).
                content(asJsonString(watchListStock))).
                andExpect(status().isBadRequest());
        verify(stockService,times(0)).updateStock(any());
    }

    public static String asJsonString(final Object obj){
        try{
            return new ObjectMapper().writeValueAsString(obj);
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }
}